//
//  DangerObjectsDetectorWorker.hpp
//  SquareDetect
//
//  Created by Dmytro Hrebeniuk on 11/14/18.
//  Copyright © 2018 SquareDetect. All rights reserved.
//

#ifndef DangerObjectsDetectorWorker_hpp
#define DangerObjectsDetectorWorker_hpp

#include <stdio.h>
#include <vector>
#include <functional>

namespace cv {
    class Mat;
}

typedef void (*ResultCallbackFunc)(cv::Mat);

class DangerObjectsDetectorWorkerPrivate;

class DangerObjectsDetectorWorker 
{
public:
	DangerObjectsDetectorWorker();
	~DangerObjectsDetectorWorker();

	void create(ResultCallbackFunc p_resultCallback);
	void start();
	void restart();
	void requestJob(
		cv::Mat &p_inputRGBImage,
		cv::Mat &p_grayImage,
		int p_offX,
		int p_offY,
		void *p_userData
	);

	void waitForFinished();
	void destroy();

	cv::Mat detectObjectsInRGBImage(const cv::Mat &inputRGBImage, const cv::Mat &grayImage, int offsetX, int offsetY);

private:
	DangerObjectsDetectorWorkerPrivate *d;
};


#endif /* DengerObjectsDetectorWorker_hpp */
