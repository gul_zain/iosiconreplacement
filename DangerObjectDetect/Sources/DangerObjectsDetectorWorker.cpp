//
//  DangerObjectsDetectorWorker.cpp
//  SquareDetect
//
//  Created by Dmytro Hrebeniuk on 11/14/18.
//  Copyright © 2018 SquareDetect. All rights reserved.
//

#include "DangerObjectsDetectorWorker.hpp"
#include <opencv2/opencv.hpp>
#include <vector>
#include <pthread.h>
#include "pevents.h"

using namespace neosmart;
using namespace cv;

#define THREAD_NUM                2
#define JOB_BUFFER_SIZE         6*THREAD_NUM
#define DETECTION_BUFFER_SIZE   6*THREAD_NUM

typedef struct Job
{
    cv::Mat m_rgbImg;
    cv::Mat m_grayImg;
    int p_offX;
    int p_offY;
    void *m_userData;
};

typedef struct DetectionResult
{
    Job *m_job;
    Mat m_resultImg;
} DetectionResult;

typedef struct DetectThreadParams
{
    int m_threadIdx;
    DangerObjectsDetectorWorkerPrivate *m_this;
} DetectThreadParams;

class DangerObjectsDetectorWorkerPrivate
{
public:
    DangerObjectsDetectorWorkerPrivate()
    {
        m_detectThreadCnt = 0;
        m_detectThreadIdx = 0;
        m_finish = false;
        m_isRunning = false;
        m_trackThread = 0;
        m_finishEvent = NULL;
        m_resultCallback = 0;
    }
    ~DangerObjectsDetectorWorkerPrivate() {};
    pthread_t m_detectThreads[THREAD_NUM];
    pthread_t m_trackThread;
    
    int m_detectThreadCnt;
    int m_detectThreadIdx;
    
    ResultCallbackFunc m_resultCallback;
    
    std::queue<Job*> m_jobs[THREAD_NUM];
    std::queue<DetectionResult*> m_detectionResult[THREAD_NUM];
    pthread_mutex_t m_jobMutex[THREAD_NUM];
    pthread_mutex_t m_interMutex[THREAD_NUM];
    
    neosmart_event_t m_jobReadyEvent[THREAD_NUM];
    neosmart_event_t m_jobAcceptableEvent[THREAD_NUM];
    neosmart_event_t m_detectAcceptableEvent[THREAD_NUM];
    neosmart_event_t m_interResultEvent[THREAD_NUM];
    
    neosmart_event_t m_finishEvent;
    bool m_finish;
    bool m_isRunning;
    
    neosmart_event_t m_detectionFinishEvent[THREAD_NUM];
    bool m_detectionFinished[THREAD_NUM];
    
    DetectThreadParams m_detectThreadParams[THREAD_NUM];
    
    void detect(int p_threadIdx);
    void track();
    
    static void* detectThreadFunc(void* p_arg);
    static void* trackThreadFunc(void* p_arg);
    
    
    std::vector<cv::Rect> createBoxes(std::vector<std::vector<cv::Point>> contours);
    cv::Mat plotBoxes(cv::Mat image, std::vector<cv::Rect> boxes, cv::Scalar color, int width);
    std::vector<std::vector<cv::Point>> createContours(const cv::Mat &contourInput, int area, int small, int offsetX, int offsetY);
    cv::Mat createHighlightROI(cv::Mat image, cv::Mat mask, cv::Scalar color, int width);
    cv::Mat detectObjectsInRGBImage(const cv::Mat &inputRGBImage, const cv::Mat &grayImage, int offsetX, int offsetY);
    cv::Mat createMaskMat(const cv::Mat &image);
    cv::Mat createBlackProcessingMaskMat(const cv::Mat &inputBlack, const cv::Mat &mask, const cv::Mat &equalized, int offsetX, int offsetY);
    cv::Mat createWhiteProcessingMaskMat(const cv::Mat &inputGray, const cv::Mat &mask, const cv::Mat &inputRGB, int offsetX, int offsetY);
    cv::Mat createReziedMask(const cv::Mat &sourceMat, const cv::Mat &destinationMat);
};

DangerObjectsDetectorWorker::DangerObjectsDetectorWorker()
{
    d = new DangerObjectsDetectorWorkerPrivate;
}

DangerObjectsDetectorWorker::~DangerObjectsDetectorWorker()
{
    delete d;
}

void DangerObjectsDetectorWorker::create(
                                         ResultCallbackFunc p_resultCallback
                                         )
{
    d->m_detectThreadCnt = THREAD_NUM;
    d->m_resultCallback = p_resultCallback;
    int w_i;
    for (w_i = 0; w_i < d->m_detectThreadCnt; w_i++)
    {
        pthread_mutex_init(&(d->m_jobMutex[w_i]), NULL);
        pthread_mutex_init(&(d->m_interMutex[w_i]), NULL);
        
        d->m_jobReadyEvent[w_i] = CreateEvent(true, false);
        d->m_jobAcceptableEvent[w_i] = CreateEvent(true, true);
        d->m_detectAcceptableEvent[w_i] = CreateEvent(true, true);
        d->m_interResultEvent[w_i] = CreateEvent(true, false);
        d->m_detectionFinishEvent[w_i] = CreateEvent(true, false);
        d->m_detectionFinished[w_i] = false;
    }
    
    d->m_finishEvent = CreateEvent(true, false);
    d->m_finish = false;
    d->m_isRunning = false;
}

void DangerObjectsDetectorWorker::start()
{
    if (d->m_isRunning)
        return;
    
    d->m_isRunning = true;
    
    int w_i;
    for (w_i = 0; w_i < d->m_detectThreadCnt; w_i++)
    {
        d->m_detectThreadParams[w_i].m_threadIdx = w_i;
        d->m_detectThreadParams[w_i].m_this = d;
        pthread_create(&(d->m_detectThreads[w_i]), 0, &DangerObjectsDetectorWorkerPrivate::detectThreadFunc,
                       (void*)&(d->m_detectThreadParams[w_i]));
    }
    pthread_create(&d->m_trackThread, 0, &DangerObjectsDetectorWorkerPrivate::trackThreadFunc, (void*)d);
    d->m_detectThreadIdx = 0;
}

void DangerObjectsDetectorWorker::restart()
{
    if (d->m_isRunning)
    {
        waitForFinished();
        start();
    }
}

void DangerObjectsDetectorWorker::requestJob(
                                             cv::Mat &p_inputRGBImage,
                                             cv::Mat &p_grayImage,
                                             int p_offX,
                                             int p_offY,
                                             void *p_userData
                                             )
{
    if (!d->m_isRunning)
        return;
    
    WaitForEvent(d->m_jobAcceptableEvent[d->m_detectThreadIdx]);
    Job *w_job = new Job;
    w_job->m_userData = p_userData;
    
    w_job->m_rgbImg = p_inputRGBImage.clone();
    w_job->m_grayImg = p_grayImage.clone();
    
    pthread_mutex_lock(&(d->m_jobMutex[d->m_detectThreadIdx]));
    d->m_jobs[d->m_detectThreadIdx].push(w_job);
    
    int w_maxJobCnt = JOB_BUFFER_SIZE / d->m_detectThreadCnt;
    if (w_maxJobCnt < 2) w_maxJobCnt = 2;
    
    if (d->m_jobs[d->m_detectThreadIdx].size() >= w_maxJobCnt)
    {
        ResetEvent(d->m_jobAcceptableEvent[d->m_detectThreadIdx]);
    }
    
    pthread_mutex_unlock(&(d->m_jobMutex[d->m_detectThreadIdx]));
    SetEvent(d->m_jobReadyEvent[d->m_detectThreadIdx]);
    
    d->m_detectThreadIdx++;
    if (d->m_detectThreadIdx >= d->m_detectThreadCnt)
        d->m_detectThreadIdx = 0;
}

void DangerObjectsDetectorWorker::waitForFinished()
{
    d->m_finish = true;
    SetEvent(d->m_finishEvent);
    
    int w_i;
    for (w_i = 0; w_i < d->m_detectThreadCnt; w_i++)
    {
        pthread_join(d->m_detectThreads[w_i], 0);
    }
    pthread_join(d->m_trackThread, 0);
    
    ResetEvent(d->m_finishEvent);
    
    for (w_i = 0; w_i < d->m_detectThreadCnt; w_i++)
    {
        ResetEvent(d->m_jobReadyEvent[w_i]);
        SetEvent(d->m_jobAcceptableEvent[w_i]);
        SetEvent(d->m_detectAcceptableEvent[w_i]);
        ResetEvent(d->m_interResultEvent[w_i]);
        ResetEvent(d->m_detectionFinishEvent[w_i]);
        d->m_detectionFinished[w_i] = false;
    }
    d->m_isRunning = false;
    d->m_finish = false;
}

void DangerObjectsDetectorWorker::destroy()
{
    if (d->m_isRunning)
        waitForFinished();
    
    int w_i;
    for (w_i = 0; w_i < d->m_detectThreadCnt; w_i++)
    {
        pthread_mutex_destroy(&d->m_jobMutex[w_i]);
        pthread_mutex_destroy(&d->m_interMutex[w_i]);
        
        DestroyEvent(d->m_jobReadyEvent[w_i]);
        DestroyEvent(d->m_jobAcceptableEvent[w_i]);
        DestroyEvent(d->m_detectAcceptableEvent[w_i]);
        DestroyEvent(d->m_interResultEvent[w_i]);
        DestroyEvent(d->m_detectionFinishEvent[w_i]);
    }
    DestroyEvent(d->m_finishEvent);
}

cv::Mat DangerObjectsDetectorWorker::detectObjectsInRGBImage(const cv::Mat &inputRGBImage, const cv::Mat &grayImage, int offsetX, int offsetY)
{
    return d->detectObjectsInRGBImage(inputRGBImage, grayImage, offsetX, offsetY);
}

void DangerObjectsDetectorWorkerPrivate::detect(int p_threadIdx)
{
    int w_threadIdx = p_threadIdx;
    while (1)
    {
        Job *w_job = NULL;
        pthread_mutex_lock(&m_jobMutex[w_threadIdx]);
        if (!m_jobs[w_threadIdx].empty())
        {
            w_job = m_jobs[w_threadIdx].front();
            m_jobs[w_threadIdx].pop();
        }
        pthread_mutex_unlock(&m_jobMutex[w_threadIdx]);
        if (w_job)
        {
            SetEvent(m_jobAcceptableEvent[w_threadIdx]);
            
            DetectionResult *w_detectionResult = new DetectionResult;
            memset(w_detectionResult, 0, sizeof(DetectionResult));
            w_detectionResult->m_job = w_job;
            
            cv::Mat w_result = detectObjectsInRGBImage(w_job->m_rgbImg, w_job->m_grayImg, w_job->p_offX, w_job->p_offY);
            
            w_detectionResult->m_resultImg = w_result.clone();
            
            WaitForEvent(m_detectAcceptableEvent[w_threadIdx]);
            
            pthread_mutex_lock(&m_interMutex[w_threadIdx]);
            m_detectionResult[w_threadIdx].push(w_detectionResult);
            
            if(m_detectionResult[w_threadIdx].size() > DETECTION_BUFFER_SIZE)
            {
                ResetEvent(m_detectAcceptableEvent[w_threadIdx]);
            }
            pthread_mutex_unlock(&m_interMutex[w_threadIdx]);
            
            SetEvent(m_interResultEvent[w_threadIdx]);
        }
        else
        {
            ResetEvent(m_jobReadyEvent[w_threadIdx]);
            
            if (m_finish)
                break;
            
            neosmart_event_t w_events[2];
            w_events[0] = m_jobReadyEvent[w_threadIdx];
            w_events[1] = m_finishEvent;
            
            WaitForMultipleEvents(w_events, 2, false, -1);
        }
    }
    
    m_detectionFinished[w_threadIdx] = true;
    SetEvent(m_detectionFinishEvent[w_threadIdx]);
}

void DangerObjectsDetectorWorkerPrivate::track()
{
    int w_threadIdx = 0;
    while (1)
    {
        DetectionResult *w_detectionResult = NULL;
        pthread_mutex_lock(&m_interMutex[w_threadIdx]);
        if (!m_detectionResult[w_threadIdx].empty())
        {
            w_detectionResult = m_detectionResult[w_threadIdx].front();
            m_detectionResult[w_threadIdx].pop();
        }
        pthread_mutex_unlock(&m_interMutex[w_threadIdx]);
        if (w_detectionResult)
        {
            SetEvent(m_detectAcceptableEvent[w_threadIdx]);
            
            m_resultCallback(w_detectionResult->m_resultImg);
            
            delete w_detectionResult->m_job;
            delete w_detectionResult;
            
            w_threadIdx++;
            if (w_threadIdx >= m_detectThreadCnt)
                w_threadIdx = 0;
        }
        else
        {
            ResetEvent(m_interResultEvent[w_threadIdx]);
            
            if (m_detectionFinished[w_threadIdx])
                break;
            
            neosmart_event_t w_events[2];
            w_events[0] = m_interResultEvent[w_threadIdx];
            w_events[1] = m_detectionFinishEvent[w_threadIdx];
            
            WaitForMultipleEvents(w_events, 2, false, -1);
        }
    }
}

void* DangerObjectsDetectorWorkerPrivate::detectThreadFunc(void *p_arg)
{
    DetectThreadParams *w_params = (DetectThreadParams*)p_arg;
    w_params->m_this->detect(w_params->m_threadIdx);
    
    return (void*)0;
}

void* DangerObjectsDetectorWorkerPrivate::trackThreadFunc(void *p_arg)
{
    DangerObjectsDetectorWorkerPrivate *w_this = (DangerObjectsDetectorWorkerPrivate*)p_arg;
    w_this->track();
    
    return (void*)0;
}

std::vector<cv::Rect> DangerObjectsDetectorWorkerPrivate::createBoxes(std::vector<std::vector<cv::Point>> contours) {
    auto boundingBoxes = std::vector<cv::Rect>();
    for(auto contour: contours) {
        cv::Rect rect = cv::boundingRect(contour);
        boundingBoxes.push_back(rect);
    }
    return boundingBoxes;
}

cv::Mat DangerObjectsDetectorWorkerPrivate::plotBoxes(cv::Mat image, std::vector<cv::Rect> boxes, cv::Scalar color, int width) {
    for(auto box: boxes) {
        cv::rectangle(image, box, color, width);
    }
    return image;
}

std::vector<std::vector<cv::Point>> DangerObjectsDetectorWorkerPrivate::createContours(const cv::Mat &contourInput, int area, int small, int offsetX, int offsetY) {
    
    cv::Mat image = contourInput;
    
    std::vector<std::vector<cv::Point>> finalContours;
    
    std::vector<std::vector<cv::Point>> contours;
    
    cv::findContours(image, contours, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);
    
    for (auto contour : contours) {
        
        std::vector<cv::Point> adjustedContour;
        for (auto point: contour) {
            int pointX = fmax(fmin(point.x + offsetX, image.cols), 0);
            int pointY = fmax(fmin(point.y + offsetY, image.rows), 0);
            adjustedContour.push_back(cv::Point(pointX, pointY));
        }
        
        double contourArea = cv::contourArea(contour);
        if (contourArea > area && !small) {
            finalContours.push_back(adjustedContour);
        }
        else if (contourArea < area && small) {
            finalContours.push_back(adjustedContour);
        }
    }
    
    return finalContours;
}


cv::Mat DangerObjectsDetectorWorkerPrivate::createHighlightROI(cv::Mat image, cv::Mat mask, cv::Scalar color, int width) {
    cv::Mat result = image;
    
    auto contours = createContours(mask, 0, false, 0, 0);
    if (contours.size() > 0) {
        auto boxes = createBoxes(contours);
        result = plotBoxes(image, boxes, color, width);
    }
    return result;
}

cv::Mat DangerObjectsDetectorWorkerPrivate::detectObjectsInRGBImage(const cv::Mat &inputRGBImage, const cv::Mat &grayImage, int offsetX, int offsetY) {
    
    cv::Mat rgbCombined;
    
    auto colorNumbers = inputRGBImage.step.buf[1];
    cv::Mat rgbImage;
    if (colorNumbers == 1)
    {
        cv::cvtColor(inputRGBImage, rgbImage, CV_GRAY2BGRA);
    }
    else if (colorNumbers == 3)
    {
        cv::cvtColor(inputRGBImage, rgbImage, CV_BGR2RGBA);
    }
    else
    {
        rgbImage = inputRGBImage;
    }
    
    cv::Mat grayImageMat;
    cv::cvtColor(grayImage, grayImageMat, CV_BGR2GRAY);
    
    cv::GaussianBlur(grayImageMat, grayImageMat, cv::Size(7, 7), 0, 0);
    
    cv::Mat equalizedMat;
    
    cv::equalizeHist(grayImageMat, equalizedMat);
    
    cv::Mat mappedGrayMat;
    
    cv::applyColorMap(equalizedMat, mappedGrayMat, cv::COLORMAP_JET);
    
    cv::Mat mask = createMaskMat(mappedGrayMat);
    
    cv::Mat maskBlack = createBlackProcessingMaskMat(mappedGrayMat, mask, equalizedMat, offsetX, offsetY);
    
    cv::Mat eqMask = equalizedMat & mask;
    
    cv::Mat rgbImageMat;
    
    cv::cvtColor(equalizedMat, rgbImageMat, cv::COLOR_GRAY2RGBA);
    
    cv::Mat whiteMask = createWhiteProcessingMaskMat(eqMask, mask, rgbImageMat, offsetY, offsetX);
    
    whiteMask = createReziedMask(whiteMask, rgbImage);
    
    cv::Mat maskBlackBGRColor[3];
    cv::split(maskBlack, maskBlackBGRColor);
    maskBlack = createReziedMask(maskBlackBGRColor[0], rgbImage);
    
    cv::Mat colorImage = inputRGBImage;
    
    uint8_t* colorImg_pixel = colorImage.ptr<uint8_t>(0, 0);
    uint8_t* blackMaskColor = maskBlack.ptr<uint8_t>(0, 0);
    uint8_t* whiteMaskColor = whiteMask.ptr<uint8_t>(0, 0);
    
    uint8_t* colorImg_endPixel = colorImg_pixel + colorImage.cols * colorImage.rows * 4;
    
    for (; colorImg_pixel != colorImg_endPixel; colorImg_pixel+=4, blackMaskColor++, whiteMaskColor++)
    {
        if (*blackMaskColor == 255)
        {
            *colorImg_pixel = 123;
            *(colorImg_pixel+1) = 233;
            *(colorImg_pixel+2) = 23;
            *(colorImg_pixel+3) = 255;
        }
        
        if (*whiteMaskColor == 255)
        {
            *colorImg_pixel = 142;
            *(colorImg_pixel+1) = 2;
            *(colorImg_pixel+2) = 222;
            *(colorImg_pixel+3) = 255;
        }
    }
    
    rgbCombined = createHighlightROI(colorImage, whiteMask, CV_RGB(0, 255, 0), 5);
    rgbCombined = createHighlightROI(rgbCombined, maskBlack,  CV_RGB(0, 0, 255), 5);
    
    return rgbCombined;
}

cv::Mat DangerObjectsDetectorWorkerPrivate::createMaskMat(const cv::Mat &image) {
    cv::Mat compomentsBGRColor[3];
    cv::split(image, compomentsBGRColor);
    
    cv::Mat redImage = compomentsBGRColor[2];
    cv::Mat contourInputMat;
    cv::GaussianBlur(redImage, contourInputMat, cv::Size(7, 7), 0);
    
    cv::threshold(contourInputMat, contourInputMat, 200, 255, CV_THRESH_BINARY);
    
    cv::morphologyEx(contourInputMat, contourInputMat, cv::MORPH_CLOSE, cv::Mat::ones(3, 3, CV_32F), cv::Point(-1, -1), 2);
    
    auto contours = createContours(contourInputMat, 20000, false, 0, 0);
    
    cv::Mat mask = cv::Mat::zeros(contourInputMat.rows, contourInputMat.cols, redImage.type());
    
#if 0
    cv::fillPoly(mask, contours, CV_RGB(255, 255, 255));
#else
    for (auto contour : contours)
        cv::fillConvexPoly(mask, contour, CV_RGB(255, 255, 255));
#endif
    
    cv::Mat maskBGRColor[3];
    cv::split(mask, maskBGRColor);
    
#if 0
    cv::morphologyEx(maskBGRColor[0], mask, cv::MORPH_CLOSE, cv::Mat::ones(13, 13, CV_32F), cv::Point(-1, -1), 5);
#else
    Mat element = getStructuringElement(MORPH_RECT, Size(13, 13), Point(-1, -1));
    cv::morphologyEx(maskBGRColor[0], mask, cv::MORPH_CLOSE, element);
#endif
    
#if 0
    for(int y = 0; y< (int)fmin(mask.rows, 50); y++) {
        for(int x = 0; x< mask.cols; x++) {
            mask.at<uchar>(cv::Point(x,y)) = 0;
        }
    }
#else
    int y = (int)fmin(mask.rows, 50) - 1;
    while (y >= 0)
    {
        uchar* pixel = mask.ptr<uchar>(y);
        memset(pixel, 0, mask.step);
        y--;
    }
#endif
    
    return mask;
}

cv::Mat DangerObjectsDetectorWorkerPrivate::createBlackProcessingMaskMat(const cv::Mat &inputBlack, const cv::Mat &mask, const cv::Mat &equalized, int offsetX, int offsetY) {
    
    cv::Mat seedsImage;
    cv::threshold(equalized, seedsImage, 4, 255, CV_THRESH_BINARY_INV);
    
    cv::Mat multiplyResult = seedsImage & mask;
    auto contours = createContours(multiplyResult, 60, false, offsetX, offsetY);
    
    cv::Mat mask1 = cv::Mat::zeros(mask.rows, mask.cols, mask.type());
    
    for (auto contour: contours) {
        std::vector<cv::Point> hull;
        cv::convexHull(contour, hull, false);
#if 0
        auto hulls = std::vector<std::vector<cv::Point>>();
        hulls.push_back(hull);
        cv::fillPoly(mask1, hulls, CV_RGB(255, 255, 255));
#else
        cv::fillConvexPoly(mask1, hull, CV_RGB(255, 255, 255));
#endif
    }
    
    return mask1;
}

cv::Mat DangerObjectsDetectorWorkerPrivate::createWhiteProcessingMaskMat(const cv::Mat &inputGray, const cv::Mat &mask, const cv::Mat &inputRGB, int offsetX, int offsetY) {
    
    cv::Mat whiteMat;
    cv::applyColorMap(inputGray, whiteMat, cv::COLORMAP_RAINBOW);
    
    cv::Mat whiteBGRColor[3];
    cv::split(whiteMat, whiteBGRColor);
    
    cv::Mat whiteGrayMat = whiteBGRColor[2];
#if 0
    for (int y = 0; y < whiteGrayMat.rows; y++)
    {
        for (int x = 0; x < whiteGrayMat.cols; x++)
        {
            uchar color = whiteGrayMat.at<uchar>(cv::Point(x, y));
            int value = (color>162 && color<175) ? 256 : color;
            if (value == 256)
            {
                color = 255;
            }
            else
            {
                color = 0;
            }
            
            whiteGrayMat.at<uchar>(cv::Point(x, y)) = color;
        }
    }
#else
    cv::inRange(whiteGrayMat, 162, 175, whiteGrayMat);
#endif
    
    auto whiteGrayContours = createContours(whiteGrayMat, 2500, true, offsetX, offsetY);
    cv::Mat whiteMask = cv::Mat::zeros(whiteMat.rows, whiteMat.cols, whiteMat.type());
#if 0
    for (auto whiteGrayContour: whiteGrayContours) {
        auto contours = std::vector<std::vector<cv::Point>>();
        contours.push_back(whiteGrayContour);
        cv::fillPoly(whiteMask, contours, CV_RGB(255, 255, 255));
    }
#else
    for (auto whiteGrayContour : whiteGrayContours)
        cv::fillConvexPoly(whiteMask, whiteGrayContour, CV_RGB(255, 255, 255));
#endif
    
    cv::Mat whiteBlueBGRColor[3];
    cv::split(whiteMask, whiteBlueBGRColor);
    
    cv::morphologyEx(whiteBlueBGRColor[0], whiteMask, cv::MORPH_OPEN, cv::Mat::ones(3, 3, CV_32F), cv::Point(-1, -1), 5);
    whiteMask = whiteMask&mask;
    
    auto whiteContours = createContours(whiteMask, 0, false, offsetX, offsetY);
    whiteMask = cv::Mat::zeros(whiteMask.rows, whiteMask.cols, whiteMask.type());
    
    for (auto whiteContour: whiteContours)
    {
        auto moments = cv::moments(whiteContour);
        auto colors = std::vector<int>();
        
        if(moments.m00 != 0.0)
        {
            int cx = moments.m10 / moments.m00;
            int cy = moments.m01 / moments.m00;
            
            for(int y = -5; y <= 5; y++)
            {
                for(int x = -5; x <= 5; x++)
                {
                    if((cx+x >= 0) && (cy+y>= 0))
                    {
                        auto color = whiteGrayMat.at<uchar>(cv::Point(cx+x, cy+y));
                        colors.push_back(color);
                    }
                }
            }
        }
        std::sort (colors.begin(), colors.end());
        if (!colors.empty())
        {
            int averageColor = colors[colors.size()/2];
            if (averageColor>225)
            {
#if 0
                auto contours = std::vector<std::vector<cv::Point>>();
                contours.push_back(whiteContour);
                cv::fillPoly(whiteMask, contours, CV_RGB(255, 255, 255));
#else
                cv::fillConvexPoly(whiteMask, whiteContour, CV_RGB(255, 255, 255));
#endif
            }
        }
    }
    
    whiteMask = whiteMask&mask;
    
#if 0
    for(int y = 0; y< (int)(whiteMask.rows*0.3); y++) {
        for(int x = 0; x< whiteMask.cols; x++) {
            whiteMask.at<uchar>(cv::Point(x,y)) = 0;
        }
    }
#else
    int y = (int)(whiteMask.rows*0.3) - 1;
    while (y >= 0)
    {
        uchar* pixel = whiteMask.ptr<uchar>(y);
        memset(pixel, 0, whiteMask.step);
        y--;
    }
#endif
    
    return whiteMask;
}

cv::Mat DangerObjectsDetectorWorkerPrivate::createReziedMask(const cv::Mat &sourceMat, const cv::Mat &destinationMat) {
    
    cv::Mat resultMat;
    cv::resize(sourceMat, resultMat, cv::Size(destinationMat.cols, destinationMat.rows));
    
    return resultMat;
}
