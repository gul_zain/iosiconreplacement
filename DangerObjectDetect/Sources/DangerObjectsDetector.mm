//
//  DangerObjectsDetector.m
//  SquareDetect
//
//  Created by Dmytro Hrebeniuk on 11/14/18.
//  Copyright © 2018 SquareDetect. All rights reserved.
//

#import "DangerObjectsDetector.h"
#include <opencv2/imgcodecs/ios.h>
#include "DangerObjectsDetectorWorker.hpp"

@implementation DangerObjectsDetector

- (UIImage *)detectObjectsInRGBImage:(UIImage *)rgbImage inGrayImage:(UIImage *)grayImage offsetSize:(CGSize)offsetSize {
    cv::Mat rgbMatImage;
    cv::Mat grayMatImage;
    
    UIImageToMat(rgbImage, rgbMatImage);
    UIImageToMat(grayImage, grayMatImage);
    
    DangerObjectsDetectorWorker *dangerObjectsDetectorWorker = new DangerObjectsDetectorWorker();
    cv::Mat result = dangerObjectsDetectorWorker->detectObjectsInRGBImage(rgbMatImage, grayMatImage, offsetSize.width, offsetSize.height);
    UIImage *resultImage = MatToUIImage(result);
    
    delete dangerObjectsDetectorWorker;
    
    return resultImage;
}

DangerObjectsDetectorWorker g_engine;

CallbackFromSwift g_callbackFromSwift;

void callbackFromC(cv::Mat resultImg)
{
    void *resultImage = (__bridge void*)MatToUIImage(resultImg);
    g_callbackFromSwift(resultImage);
}

+ (void) createEngineWrapper: (CallbackFromSwift) callbackFromSwift
{
    g_engine.create((ResultCallbackFunc)callbackFromC);
    g_callbackFromSwift = callbackFromSwift;
}

+ (void) startWrapper
{
    g_engine.start();
}

+ (void) requestJobWrapper: (UIImage *)rgbImage inGrayImage:(UIImage *)grayImage offsetSize:(CGSize)offsetSize
{
    cv::Mat rgbMatImage;
    cv::Mat grayMatImage;
    
    UIImageToMat(rgbImage, rgbMatImage);
    UIImageToMat(grayImage, grayMatImage);
    
    g_engine.requestJob(rgbMatImage, grayMatImage, offsetSize.width, offsetSize.height, 0);
    
    rgbMatImage.release();
    grayMatImage.release();
}
+ (void) waitFinishedWrapper
{
    g_engine.waitForFinished();
}
+ (void) destroyWrapper
{
    g_engine.destroy();
}

@end
