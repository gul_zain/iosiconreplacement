//
//  ViewController.m
//  RealtimeVideoFilter
//
//  Created by Altitude Labs on 23/12/15.
//  Copyright © 2015 Victor. All rights reserved.
//

#import "CameraController.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreImage/CoreImage.h>
#import <GLKit/GLKit.h>
#import <DangerObjectDetect/DangerObjectsDetector.h>
#include <opencv2/imgcodecs/ios.h>

@interface CameraController () <AVCaptureVideoDataOutputSampleBufferDelegate>

@property GLKView *videoPreviewView;
@property CIContext *ciContext;
@property EAGLContext *eaglContext;
@property CGRect videoPreviewViewBounds;

@property AVCaptureDevice *videoDevice;
@property AVCaptureSession *captureSession;
@property dispatch_queue_t captureSessionQueue;


@property (strong, nonatomic) IBOutlet UIImageView *visualYCbCrView;

@property (strong, nonatomic) UIImage *thermalImage;
@property (strong, nonatomic) UIImage *visualYCbCrImage;

@end

//#define USE_MT

#ifdef USE_MT
UIImageView *g_visualYCbCrView;

void callbackForResult(void* resultImg)
{
    UIImage *w_resultImg = (__bridge UIImage*)resultImg;
    
    UIImage *w_visualYCbCrImage = [[UIImage alloc]
                             initWithCGImage:w_resultImg.CGImage
                             scale:1.0
                             orientation:UIImageOrientationRight];
    
    dispatch_async( dispatch_get_main_queue(), ^{
        [g_visualYCbCrView setImage: w_visualYCbCrImage];
    });
}
#endif

@implementation CameraController

- (void)viewDidLoad {
    [super viewDidLoad];
    
#ifdef USE_MT
    [DangerObjectsDetector createEngineWrapper : callbackForResult];
#endif
    
    [self _start];
}


- (void)_start
{
    // get the input device and also validate the settings
    NSArray *videoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    
    AVCaptureDevicePosition position = AVCaptureDevicePositionBack;
    
    for (AVCaptureDevice *device in videoDevices)
    {
        if (device.position == position) {
            _videoDevice = device;
            break;
        }
    }
    
    // obtain device input
    NSError *error = nil;
    AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:_videoDevice error:&error];
    if (!videoDeviceInput)
    {
        NSLog(@"%@", [NSString stringWithFormat:@"Unable to obtain video device input, error: %@", error]);
        return;
    }
    
    // obtain the preset and validate the preset
    NSString *preset = AVCaptureSessionPresetHigh;
    if (![_videoDevice supportsAVCaptureSessionPreset:preset])
    {
        NSLog(@"%@", [NSString stringWithFormat:@"Capture session preset not supported by video device: %@", preset]);
        return;
    }
    
    // create the capture session
    _captureSession = [[AVCaptureSession alloc] init];
    _captureSession.sessionPreset = preset;
    
    // CoreImage wants BGRA pixel format
    NSDictionary *outputSettings = @{ (id)kCVPixelBufferPixelFormatTypeKey : [NSNumber numberWithInteger:kCVPixelFormatType_32BGRA]};
    // create and configure video data output
    AVCaptureVideoDataOutput *videoDataOutput = [[AVCaptureVideoDataOutput alloc] init];
    videoDataOutput.videoSettings = outputSettings;
    
    // create the dispatch queue for handling capture session delegate method calls
    _captureSessionQueue = dispatch_queue_create("capture_session_queue", NULL);
    [videoDataOutput setSampleBufferDelegate:self queue:_captureSessionQueue];
    
    videoDataOutput.alwaysDiscardsLateVideoFrames = YES;
    
    // begin configure capture session
    [_captureSession beginConfiguration];
    
    if (![_captureSession canAddOutput:videoDataOutput])
    {
        NSLog(@"Cannot add video data output");
        _captureSession = nil;
        return;
    }
    
    // connect the video device input and video data and still image outputs
    [_captureSession addInput:videoDeviceInput];
    [_captureSession addOutput:videoDataOutput];
    
    [_captureSession commitConfiguration];
    
    // then start everything
    [_captureSession startRunning];
    
#ifdef USE_MT
    [DangerObjectsDetector startWrapper];
#endif
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection {
    
    CGImageRef cgImage = [self imageFromSampleBuffer:sampleBuffer];
    self.visualYCbCrImage = [UIImage imageWithCGImage: cgImage ];
    self.thermalImage = [UIImage imageNamed:@"gray1.jpg"];
   
#ifndef USE_MT
    DangerObjectsDetector *dangerObjectsDetector = [[DangerObjectsDetector alloc] init];
    UIImage *result = [dangerObjectsDetector detectObjectsInRGBImage:self.visualYCbCrImage inGrayImage: self.thermalImage offsetSize:CGSizeMake(0, 0) ];
    
    self.visualYCbCrImage = [[UIImage alloc]
                             initWithCGImage:result.CGImage
                             scale:1.0
                             orientation:UIImageOrientationRight];
    
    CGImageRelease( cgImage );
    
    dispatch_async( dispatch_get_main_queue(), ^{
        [self.visualYCbCrView setImage:self.visualYCbCrImage]; 
    });
#else
    g_visualYCbCrView = self.visualYCbCrView;
    [DangerObjectsDetector requestJobWrapper:self.visualYCbCrImage inGrayImage:self.thermalImage offsetSize:CGSizeMake(0, 0)];
    
    CGImageRelease( cgImage );
#endif
}

- (CGImageRef) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer
{
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(imageBuffer,0);
    uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef newContext = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGImageRef newImage = CGBitmapContextCreateImage(newContext);
    CGContextRelease(newContext);
    
    CGColorSpaceRelease(colorSpace);
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    return newImage;
}



@end
