//
//  UIImageExtensions.swift
//  DangerObjectDetectSample
//
//  Created by Dmytro Hrebeniuk on 12/13/18.
//  Copyright © 2018 SquareDetect. All rights reserved.
//

import UIKit


extension UIImage {
    
    var grayScale: UIImage {
        
        let imgRect = CGRect(origin: .zero, size: size)
        
        let colorSpace = CGColorSpaceCreateDeviceGray()
        
        let context = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue).rawValue)
        
        self.cgImage.map { context?.draw($0, in: imgRect) }
        
        let imageRef = context!.makeImage()
        let newImg = UIImage(cgImage: imageRef!)
        
        return newImg
    }
    
    func scale(to scale: CGFloat) -> UIImage? {
        let newSize = CGSize(width: self.size.width*scale, height: self.size.height*scale)
        
        UIGraphicsBeginImageContextWithOptions(newSize, true, 0.5)
        self.draw(in: CGRect(origin: .zero, size: newSize))
        let reziedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return reziedImage
    }
    
}
