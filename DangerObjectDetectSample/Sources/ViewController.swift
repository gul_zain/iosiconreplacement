//
//  ViewController.swift
//  SquareDetect
//
//  Created by Dmytro Hrebeniuk on 11/14/18.
//  Copyright © 2018 SquareDetect. All rights reserved.
//

import UIKit
import DangerObjectDetect

func callbackSwift( _ p : UnsafeMutableRawPointer? )->Void
{
   
}

class ViewController: UIViewController {

    @IBOutlet weak var timeLog: UITextView!
    private let dangerObjectsDetector = DangerObjectsDetector()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let directoryURL = Bundle.main.url(forResource: "TestImages", withExtension: nil) else {
            return
        }
        
        let files = try? FileManager.default.contentsOfDirectory(atPath: directoryURL.path)
        guard let grayFiles = (files?.filter { $0.contains("gray")} )?.sorted() else {
            return
        }
        var timeText = ""

//        var rgbImages = [UIImage]()
//        var grayImages = [UIImage]()
//        for (index, grayFile) in grayFiles.enumerated()
//        {
//            let rgbFile = grayFile.replacingOccurrences(of: "gray", with: "rgb")
//            let grayImageURL = directoryURL.appendingPathComponent(grayFile)
//            let rgbImageURL = directoryURL.appendingPathComponent(rgbFile)
//            let rgbImage = UIImage(contentsOfFile: rgbImageURL.path)?.scale(to: 0.5)
//            let grayImage = UIImage(contentsOfFile: grayImageURL.path)
//            rgbImages.append(rgbImage!)
//            grayImages.append(grayImage!)
//        }
//        DangerObjectsDetector.createEngineWrapper(callbackSwift)
//        
//        let timeStampStart = Date().timeIntervalSince1970
//        DangerObjectsDetector.startWrapper()
//        let cnt = rgbImages.count
//        for idx in 0 ... cnt-1
//        {
//            DangerObjectsDetector.requestJobWrapper(rgbImages[idx], inGrayImage: grayImages[idx], offsetSize: CGSize(width: 0, height: 0))
//        }
//        DangerObjectsDetector.waitFinishedWrapper()
//        let timeStampEnd = Date().timeIntervalSince1970
//        var time = Int(Double(timeStampEnd - timeStampStart) / Double(cnt) * 1000);
//        let timeStr = "average time per image: \(time) ms"
//        timeLog.text = timeStr
//        
//        DangerObjectsDetector.destroyWrapper()
    }
}
